<?php
/**
 * @file
 * gdlc_messages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gdlc_messages_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function gdlc_messages_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function gdlc_messages_node_info() {
  $items = array(
    'message' => array(
      'name' => t('Message'),
      'base' => 'node_content',
      'description' => t('Use for Messages or Sermons. It is meant for both Pre service and for the Post service archives.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'message_series' => array(
      'name' => t('Message Series'),
      'base' => 'node_content',
      'description' => t('Use for creating a grouping of Messages and adding a page that describes the Series.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
