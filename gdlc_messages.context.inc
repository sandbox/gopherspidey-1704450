<?php
/**
 * @file
 * gdlc_messages.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gdlc_messages_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'messages';
  $context->description = '';
  $context->tag = 'GDLC';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'message' => 'message',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-message_video-block' => array(
          'module' => 'views',
          'delta' => 'message_video-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-series_block-block' => array(
          'module' => 'views',
          'delta' => 'series_block-block',
          'region' => 'sidebar_first',
          'weight' => '-27',
        ),
        'views-message-block' => array(
          'module' => 'views',
          'delta' => 'message-block',
          'region' => 'sidebar_first',
          'weight' => '-26',
        ),
        'views-message_series-block' => array(
          'module' => 'views',
          'delta' => 'message_series-block',
          'region' => 'sidebar_first',
          'weight' => '-25',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('GDLC');
  $export['messages'] = $context;

  return $export;
}
