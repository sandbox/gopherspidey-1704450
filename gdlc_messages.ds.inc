<?php
/**
 * @file
 * gdlc_messages.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function gdlc_messages_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|message_series|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'message_series';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['node|message_series|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|message|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'message';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '10',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|message|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function gdlc_messages_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|message_series|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'message_series';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'field_graphic',
        1 => 'title',
        2 => 'body',
        3 => 'field_message_view',
      ),
    ),
    'fields' => array(
      'field_graphic' => 'ds_content',
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_message_view' => 'ds_content',
    ),
    'classes' => array(
      'ds_content' => array(
        'custom-bgcolor' => 'custom-bgcolor',
      ),
    ),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|message_series|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|message|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'message';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'field_graphic',
        1 => 'title',
      ),
      'left' => array(
        0 => 'field_date',
        1 => 'field_pastor_ref',
        2 => 'field_message_series_ref',
        3 => 'field_message_keywords',
        4 => 'field_audio_recording',
      ),
      'right' => array(
        0 => 'field_resources',
      ),
      'footer' => array(
        0 => 'field_intro',
        1 => 'field_transcript',
        2 => 'links',
      ),
    ),
    'fields' => array(
      'field_graphic' => 'header',
      'title' => 'header',
      'field_date' => 'left',
      'field_pastor_ref' => 'left',
      'field_message_series_ref' => 'left',
      'field_message_keywords' => 'left',
      'field_audio_recording' => 'left',
      'field_resources' => 'right',
      'field_intro' => 'footer',
      'field_transcript' => 'footer',
      'links' => 'footer',
    ),
    'classes' => array(),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|message|full'] = $ds_layout;

  return $export;
}
